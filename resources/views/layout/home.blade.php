@include('layout.misc')

@section('home')

<section id="home" class="wow fadeIn">
    <div class="home-container">

        <h1>
            @if(
                isset($_COOKIE[$cookieName]) &&
                filled($_COOKIE[$cookieName]) &&
                ($_COOKIE[$cookieName] === $cookieContent)
                    )
                @yield('clock') {{ "we're glad to have you back" }}
                </h1>
                @yield('welcome-back')
            @else
                @yield('clock') {{ "welcome to $companyname" }}
                </h1>
                @yield('slogan')
                <?php setcookie($cookieName, $cookieContent, $cookieExpire); ?>               
            @endif
        
        <img src="img/hero-img.png" alt="Hero Imgs">
        <a href="#get-started" class="btn-get-started scrollto">Get Started</a>
        <div class="btns">
            <a href="#"><i class="fa fa-apple fa-3x"></i> App Store</a>
            <a href="#"><i class="fa fa-play fa-3x"></i> Google Play</a>
            <a href="#"><i class="fa fa-windows fa-3x"></i> windows</a>
        </div>
    </div>
</section>

@endsection