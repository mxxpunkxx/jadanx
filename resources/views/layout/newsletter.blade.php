@section('newsletter')

<section id="newsletter" class="newsletter text-center wow fadeInUp">
    <div class="overlay padd-section">
        <div class="container">

            <div class="row justify-content-center">
                <div class="col-md-9 col-lg-6">

                    {{ Form::open([
                    'action'        =>      'MailController@subscribe',
                    'class'         =>      'form-inline',
                    'method'        =>      'POST'
                    ]) }}

                    {{ Form::email('email_one', $value = null, $attributes = array(

                    'placeholder'   =>      'Email Address',
                    'pattern'       =>      "[^@\s]+@[^@\s]+\.[^@\s]+",
                    'title'         =>      "hello@example.com",
                    'class'         =>      'form-control',
                    'required'      =>      'true',
                    )) }}

                    <button type="submit" name="submit" class="btn btn-default">
                        <i class="fa fa-location-arrow"></i>Subscribe</button>

                    {{ Form::close() }}

                </div>
            </div>

            @yield('div-social-2')

        </div>
    </div>
</section>

@endsection