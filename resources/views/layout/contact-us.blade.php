@section('contact-us')

<section id="contact" class="padd-section wow fadeInUp">

    <div class="container">
        <div class="section-title text-center">
            <h2>Contact</h2>
            <p class="separator">Do you have a suggestion, complaint or query? You can drop a message below</p>

                <p>
                @if ($errors->any())
                <div class="alert alert-danger alert-dismissible">
                <ul>
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <li>{{ $error }}</li>
                @endforeach
                    </ul>
                    </div>
                @endif
                </div>
                </p>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">

            <div class="col-lg-3 col-md-4">

                <div class="info">
                    <div>
                        <i class="fa fa-map-marker"></i>
                        @yield('address')
                    </div>

                    <div class="email">
                        <i class="fa fa-envelope"></i>
                        @yield('email')
                    </div>

                    <div>
                        <i class="fa fa-phone"></i>
                        @yield('phone')
                    </div>
                </div>

                @yield('div-social')

            </div>

            <div class="col-lg-5 col-md-8">
                <div class="form">
                    <div id="sendmessage">Your message has been sent. Thank you!</div>
                    <div id="errormessage"></div>
                         
                    {{ Form::open([
                    'action'    =>      'MailController@contactForm',
                    'class'     =>      'form',
                    'method'    =>      'POST',
                    'name'      =>      'form',
                    'id'        =>      'form',
                    'files'     =>       true,
                    ]) }}

                    <div class="form-group">

                        {{ Form::text('name', $value = NULL, $attributes = array(

                        'id'            =>      'name',
                        'placeholder'   =>      'Your Name',
                        'class'         =>      'form-control',
                        'minlength'     =>       4,
                        'required'      =>      'true',
                        'pattern'       =>      '[^\s][a-zA-Z0-9À-ž\s\'\-]+',
                        'title'         =>      'Some Name',
                        )) }}

                        @yield('div')

                        {{ Form::email('email_two', $value = NULL, $attributes = array(

                        'id'            =>      'email_two',
                        'placeholder'   =>      'Your Email',
                        'pattern'       =>      "[^@\s]+@[^@\s]+\.[^@\s]+",
                        'title'         =>      "hello@example.com",
                        'class'         =>      'form-control',
                        'required'      =>      'true',
                        )) }}

                        @yield('div')

                        {{ Form::text('subject', $value, $attributes = array(

                        'id'            =>      'subject',
                        'placeholder'   =>      'Subject',
                        'class'         =>      'form-control',
                        'required'      =>      'true',
                        'minlength'     =>      8,
                        )) }}

                        @yield('div')

                        {{ Form::textarea('message', $value, $attributes = array(

                        'id'            =>      'message',
                        'placeholder'   =>      'Message ± File',
                        'class'         =>      'form-control',
                        'rows'          =>       5,
                        'required'      =>       'true',
                        )) }}

                        {{ Form::file('file') }}

                        <div class="validation"></div>

                    </div>

                    <div class="text-left">
                        <button type="submit" name="submit">Send Message</button>
                    </div>

                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
    <p>

    </p>
    
</section>

@endsection