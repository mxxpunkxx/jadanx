@section('footer-script')

      <!-- JavaScript Libraries -->
      <script src="lib/jquery/jquery-3.3.1.min.js"></script>
      <script src="lib/jquery/jquery-migrate.min.js"></script>
      <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="lib/superfish/hoverIntent.js"></script>
      <script src="lib/superfish/superfish.min.js"></script>
      <script src="lib/easing/easing.min.js"></script>
      <script src="lib/modal-video/js/modal-video.js"></script>
      <script src="lib/owlcarousel/owl.carousel.min.js"></script>
      <script src="lib/wow/wow.min.js"></script>
      
      <!-- Contact Form JavaScript File -->
      <script src="contactform/contactform.js"></script>

      <!-- Template Main Javascript File -->
      <script src="{{ mix('js/app.js') }}"></script>

      {{-- <script src="js/clock.js"></script> --}}

      <!-- Web Fonts -->
      <script src="lib/webfont/1.6.26/webfont.js"></script>
      <script>
      WebFont.load({
      google: {
            families: ['Open Sans', 'Roboto', 'Philosopher', 'Miriam Libre']
            // families: ['Open Sans:300,300i,400,400i,700,700i', 'Roboto:100,300,400,500,700', 'Philosopher:400,400i,700,700i', 'Miriam Libre:400,700']
      }
      });
      </script>

@endsection