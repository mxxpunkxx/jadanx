<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $connection = 'mysql',  $table = 'items',  $primaryKey = 'id';
    protected $fillable = array(
        'name',
        'type',
        'quantity',
        'description',
    );

    public $timestamps = true;
}