<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

function quickStore($item, Request $request)
{            
    $item->name         = title_case($request->name);
    $item->type         = strtoupper($request->type);
    $item->quantity     = $request->quantity;
    $item->description  = title_case($request->description);
    $item->save();
    $request->flush();
}

class ItemController extends Controller
{
    public function index()
    {
        $item = Item::cursor();
        return view('items.index', ['item' => $item]);
    }

    public function create()
    {
        return view('items.create');
    }

    public function store(Request $request)
    {
        $item = new Item;
        quickStore($item, $request);
        return redirect()->route('items.show', $item->id)
        ->with('successCreate', 'Item has been created.');

    }

    public function show($id)
    {
        $item = Item::where('id', $id)->get();
        return view('items.show', ['item' => $item]);
    }

    public function edit($id)
    {
        $item = Item::find($id);
        return view('items.edit', ['item' => $item]);
    }

    public function update(Request $request, $id)
    {
        $item = Item::find($id);
        quickStore($item, $request);
        return redirect()->route('items.show', $item->id)
        ->with('successUpdate', 'Item has been updated.');

    }

    public function destroy($id)
    {
        $item = Item::find($id)->delete();
        return redirect()->route('items.index')
        ->with('successDestroy', 'Item has been deleted.');
    }
}