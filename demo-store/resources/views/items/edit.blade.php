@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ title_case('you can update existing items here. All fields are required,
                    but can be left as-is.') }}</div>

                @if (session('status'))
                <div class="card-body">
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

    <br>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Item Property Edit') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('items.update', $item->id) }}">
                        @method('PUT')
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('New Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" required value="{{ $item->name }}"
                                    autofocus aria-describedby="nameHelpBlock">
                                <small id="nameHelpBlock" class="form-text text-muted">
                                    Letters, numbers and spaces only. No special characters.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label text-md-right">{{ __('New Type') }}</label>

                            <div class="col-md-6">
                                <input id="type" type="text" class="form-control" name="type" required value="{{ $item->type }}"
                                    aria-describedby="typeHelpBlock">
                                <small id="typeHelpBlock" class="form-text text-muted">
                                    Letters, numbers and spaces only. No special characters.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="quantity" class="col-md-4 col-form-label text-md-right">{{ __('New Quantity')
                                }}</label>

                            <div class="col-md-6">
                                <input id="quantity" type="number" class="form-control" required value="{{ $item->quantity }}"
                                    name="quantity" aria-describedby="quantityHelpBlock">
                                <small id="quantityHelpBlock" class="form-text text-muted">
                                    Positive numbers only. No spaces, special characters.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('New
                                Description') }}</label>

                            <div class="col-md-6">
                                <textarea name="description" id="description" rows="6" required class="form-control"
                                    aria-describedby="descriptionHelpBlock">{{ $item->description }}</textarea>
                                <small id="descriptionHelpBlock" class="form-text text-muted">
                                    Letters, numbers and spaces only. No special characters.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Update This Item') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
