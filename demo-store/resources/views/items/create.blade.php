@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ title_case('you can create new items here to display. all
                    fields are required.') }}</div>

                @if (session('status'))
                <div class="card-body">
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

    <br>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Items Properties') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('items.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" placeholder="e.g. Mission Impossible 4: Fallout"
                                    name="name" value="" required autofocus aria-describedby="nameHelpBlock">
                                <small id="nameHelpBlock" class="form-text text-muted">
                                    Letters, numbers and spaces only. No special characters.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label text-md-right">{{ __('Type') }}</label>

                            <div class="col-md-6">
                                <input id="type" type="text" class="form-control" placeholder="e.g. DVD, Novel, Textbook, USB"
                                    name="type" value="" required aria-describedby="typeHelpBlock">
                                <small id="typeHelpBlock" class="form-text text-muted">
                                    Letters, numbers and spaces only. No special characters.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="quantity" class="col-md-4 col-form-label text-md-right">{{ __('Quantity') }}</label>

                            <div class="col-md-6">
                                <input id="quantity" type="number" class="form-control" title="Enter digits only"
                                    placeholder="How many of this item are in stock?" name="quantity" required
                                    aria-describedby="quantityHelpBlock">
                                <small id="quantityHelpBlock" class="form-text text-muted">
                                    Positive numbers only. No spaces, special characters.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description')
                                }}</label>

                            <div class="col-md-6">
                                <textarea name="description" id="description" rows="6" class="form-control" placeholder="Add a production description here"
                                    required aria-describedby="descriptionHelpBlock"></textarea>
                                <small id="descriptionHelpBlock" class="form-text text-muted">
                                    Letters, numbers and spaces only. No special characters.
                                </small>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary btn-block .sr-only">
                                    {{ __('Create New Item') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
