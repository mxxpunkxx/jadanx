@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ title_case('you can delete existing items here') }}</div>

                    @if (session('status'))
                        <div class="card-body">
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        </div>
                    @endif
            </div>
        </div>
    </div>

    <br>

    <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Item Removal') }}</div>
    
                    <div class="card-body">
                        <form method="POST" action="{{ route('destroyItem') }}">
                            @csrf

                            <div class="form-group row">
                                    <label for="id" class="col-md-4 col-form-label text-md-right">{{ __('I Want To Delete') }}</label>
        
                                    <div class="col-md-6">
                                        <select name="id" class="form-control" id="id" autofocus required>
                                            <option value="" disabled selected>Choose Item</option>
                                            @foreach ($items as $item)
                                            <option value="{{ $item['id'] }}"> {{ $item['name'] . ' : ' . $item['type'] }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <br>
    
                            
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Delete') }}   
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>

@endsection