@extends('layouts.app')

@section('content')
<div class="container">
    
    @if(session('successDestroy'))
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="alert alert-danger">
                    {{ session('successDestroy') }}
                </div>
            </div>
        </div>
    @else
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">You will find a list of all items in the store here. Have fun!</div>
    
                    @if (session('status'))
                    <div class="card-body">
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div> <br>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Item(s) List: </div>

                <div class="card-body">

                    @if(!filled($item))
                    <p>
                        {!! __('You are out of stock on all items.<br> Now is a good time to restock.') !!}
                    </p>
                    <p>
                        <a href="{{ route('createItem') }}">Click Here</a>{{ ' to restock inventory.' }}
                    </p>
                    @else

                    <div class="row">

                            <div class="col-md-2" style="text-align: center;">
                                {{ Form::open([
                                'method' => 'GET',
                                'route' => ['items.create']
                                ])
                                }}
                                {{ Form::submit('Add New Item') }}
                                {{ Form::close() }}
                            </div>
    
                        </div>

                    <hr>

                    @foreach ($item as $item)

                    <div class="container">

                        <div class="row">
                            <div class="col-md-4">
                                <a href="#">Entry #:</a> <br>
                                <a href="#">Name:</a> <br>
                                <a href="#">Type:</a> <br>
                                <a href="#">Quantity:</a> <br>
                                <a href="#">Description:</a><br><br>

                                <div>
                                    <small id="actionBlock" class="form-text text-muted">

                                        <div class="row">

                                            <div class="col-md-2">

                                                {{ Form::open([
                                                'method' => 'GET',
                                                'route' => ['items.edit', $item->id]
                                                ])
                                                }}
                                                {{ Form::submit('Edit') }}
                                                {{ Form::close() }}

                                            </div>

                                            <div class="col-md-2">

                                                {{ Form::open([
                                                'method' => 'DELETE',
                                                'route' => ['items.destroy', $item->id]
                                                ])
                                                }}
                                                {{ Form::submit('Delete') }}
                                                {{ Form::close() }}

                                            </div>

                                        </div>

                                    </small>

                                </div>

                            </div>
                            <div class="col-md-4">
                                {{ $item->id }} <br>
                                {{ $item->name }} <br>
                                {{ $item->type }} <br>
                                {{ $item->quantity }} <br>
                                {{ $item->description }} <br><br>

                            </div>
                        </div>

                        <hr>
                    </div>

                    @endforeach

                    <div class="row">

                        <div class="col-md-2">
                            {{ Form::open([
                            'method' => 'GET',
                            'route' => ['items.create']
                            ])
                            }}
                            {{ Form::submit('Add New Item') }}
                            {{ Form::close() }}
                        </div>

                    </div>

                    @endif

                </div>
            </div>
        </div>
    </div>

</div>

@endsection
