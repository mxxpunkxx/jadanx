@extends('layouts.app')

@section('content')
<div class="container">
        
    @if(session('successCreate'))
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="alert alert-success">
                        {{ session('successCreate') }}
                    </div>
                </div>
            </div>
    @elseif(session('successUpdate'))
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="alert alert-success">
                        {{ session('successUpdate') }}
                    </div>
                </div>
            </div>
    @endif
    
    @if (session('status'))
        <div class="card-body">
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        </div>
    @endif
        
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Item Properties </div>

                <div class="card-body">

                    <div class="container">

                        <div class="row">
                            <div class="col-md-4">
                                <a href="#">ID#:</a> <br>
                                <a href="#">Name:</a> <br>
                                <a href="#">Type:</a> <br>
                                <a href="#">Quantity:</a> <br>
                                <a href="#">Description:</a><br><br>
                            </div>
                            <div class="col-md-4">
                                @foreach ($item as $item)
                                {{ $item->id }} <br>
                                {{ $item->name }} <br>
                                {{ $item->type }} <br>
                                {{ $item->quantity }} <br>
                                {{ $item->description }} <br><br>
                                @endforeach
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="card">
                <div class="card-header">

                    <small id="actionBlock" class="form-text text-muted">


                        <div class="row">
                            <div class="col-md-2">
                                {{ Form::open([
                                'method' => 'GET',
                                'route' => ['items.index']
                                ])
                                }}
                                {{ Form::submit('View All Items') }}
                                {{ Form::close() }}
                            </div>
                            <div class="col-md-2">
                                {{ Form::open([
                                'method' => 'GET',
                                'route' => ['items.create']
                                ])
                                }}
                                {{ Form::submit('Add New Item') }}
                                {{ Form::close() }}
                            </div>

                            <div class="col-md-2">
                                {{ Form::open([
                                'method' => 'GET',
                                'route' => ['items.edit', $item->id]
                                ])
                                }}
                                {{ Form::submit('Edit This Item') }}
                                {{ Form::close() }}
                            </div>


                            <div class="col-md-2">
                                {{ Form::open([
                                'method' => 'DELETE',
                                'route' => ['items.destroy', $item->id]
                                ])
                                }}
                                {{ Form::submit('Delete') }}
                                {{ Form::close() }}
                            </div>
                        </div>

                    </small>



                </div>
            </div>
        </div>
    </div>

</div>

@endsection
