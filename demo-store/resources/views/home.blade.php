@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Welcome to your Dashboard. You're logged in!</div>

                @if (session('status'))
                <div class="card-body">
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    <br><br>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Item Tools</div>

                <div class="card-body">

                    <a href="{{ route('items.index') }}">View All Items</a> <br>
                    <a href="items/create">Create New Item</a>
                </div>
            </div>
        </div>
    </div>

    <br><br>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Users Tools: <i>Awaiting Deployment</i></div>

                <div class="card-body">

                    <a href="/users/view">View All Users</a> <br>
                    <a href="/users/create">Create New User</a>
                </div>
            </div>
        </div>
    </div>

    <br><br>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Maintenance Mode: <i>Awaiting Deployment</i></div>

                <div class="card-body">

                    <a href="/maint-on">Turn On </a> <br>
                    <a href="/maint-off">Turn Off </a>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
