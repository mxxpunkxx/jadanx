<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// throttle requests
Route::middleware('throttle:200,1')->group(function () {

    // show homepage
    Route::get('/', 'HomeController@showIndex')->name('index');

    Route::get('/{url}', function ($url){
        return redirect()->route('index');
    })->where(['url' => 'index|index.html|home|welcome']);

    // show email success page
    Route::get('/emailsent', 'HomeController@emailSuccess');
    
    // redirect undefined requests to 404
    Route::fallback('HomeController@showError404');

});

// newsletter & contact form
Route::post('/subscribe', 'MailController@subscribe')->name('subscribe');
Route::post('/contact-us', 'MailController@contactForm')->name('contact-us');