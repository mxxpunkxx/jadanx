<?php

namespace App\Mail\ContactForm;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class NoFileMailable extends Mailable
{
      private $form;

      use Queueable, SerializesModels;

      /**
       * Create a new message instance.
       *
       * @return void
       */

      public function __construct(Object $form)
      {
            $this->form       =     $form;
      }

      /**
       * Build the message.
       *
       * @return $this
       */

      public function build()
      {
            $name       =     title_case($this->form->name);
            $email      =     $this->form->email;
            $message    =     $this->form->message;
            $subject    =     title_case($this->form->subject);
            
            return $this->markdown('emails.contactform', ['message' => $message])
                        ->subject($subject)
                        ->from($email, $name)
                        ->replyTo($email, $name)
                        ->priority(1);
      }
}