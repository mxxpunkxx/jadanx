<?php

namespace App\Mail\ContactForm;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WithFileMailable extends Mailable
{
      private $form, $filename, $path;

      use Queueable, SerializesModels;

      /**
       * Create a new message instance.
       *
       * @return void
       */

      public function __construct(Object $form, $filename, $path)
      {
        $this->form         =       $form;
        $this->filename     =       $filename;
        $this->path         =       $path;      
      }

      /**
       * Build the message.
       *
       * @return $this
       */

      public function build()
      {
            $name       =     title_case($this->form->name);
            $email      =     $this->form->email;
            $message    =     $this->form->message;
            $subject    =     title_case($this->form->subject);
            $filename   =     $this->filename;
            $path       =     $this->path;
            
            return $this->markdown('emails.contactform', [
                                                        'message'   =>  $message,
                                                        'filename'  =>  $filename,
                                                        'path'      =>  $path,
                                                        ])
                        ->subject($subject)
                        ->from($email, $name)
                        ->replyTo($email, $name)
                        ->attachFromStorage($path)
                        ->priority(1);
      }
}