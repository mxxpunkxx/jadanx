<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewsletterMail extends Mailable
{

    use Queueable, SerializesModels;

    // data to be used in view

    private $email;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email =  $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email  =   $this->email;

        return $this->markdown('emails.newsletter', ['email' => $email])
                    ->subject('Your subscription was successful')
                    ->replyTo('enquiries@blueplush.com', 'Blue Plush');
    }
}