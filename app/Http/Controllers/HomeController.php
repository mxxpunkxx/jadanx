<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Cookie;

class HomeController extends Controller
{

    // method to show homepage
    public function showIndex()
    {
        $companyname    =       title_case(config('app.name'));

        // setting simple cookie
        $cookieName           =       'blue_plush_cookie';
        $str                  =       hash('WHIRLPOOL', 'WeLcOmE_tO_BlUePluSh!!!');
        $cookieContent        =       crypt($str, '$2a$07$'.$str.'$');
        $cookieExpire         =       time()+1209600; // 2 weeks
        
        return view('master', [
                                'companyname'       =>      $companyname,
                                'cookieName'        =>      $cookieName,
                                'cookieContent'     =>      $cookieContent,
                                'cookieExpire'      =>      $cookieExpire,
                            ]);
    }

    //show success page after email delivery
    public function emailSuccess(Request $request)
    {
        $email          =       $request->old('email_one');
        $name           =       $request->old('name');
        $name           =       title_case($name);

        return view('email-success.master', [
                                'name'              =>      $name,
                                'email'             =>      $email,
                                ]);
    }

    // fallback for undefinded routes
    public function showError404()
    {
        return abort(404);
    }

    // authi:api user middleware
    public function AuthApiUser(Request $request)
    {
        return $request->user();
    }
}