<?php

namespace App\Http\Controllers;

use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Jobs\SendNewsletterJob;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\MessageBag;
use App\Jobs\ContactForm\NoFileJob;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ContactForm\WithFileJob;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class MailController extends Controller
{
    private $request;
    
    //method to send the subscriber email
    public function subscribe(Request $request)
    {
        if ($request->isMethod('post')) {
            // grab POST data varaiables
            if ($request->has('email_one') && $request->filled('email_one')) {
                $request->flashOnly('email_one');

                $newsletter         =           (Object) array(
                                                'email' => $request->email_one
                                                );
                // dispatch to queue handler                
                SendNewsletterJob::dispatch($newsletter)
                                        ->delay(now()->addSeconds(5));

                return redirect('emailsent');
                $request->session()->flush();
                $request->session()->regenerate();
            }
        }
    }

    //method to send the contact form
    public function contactForm(Request $request)
    {
        if ($request->isMethod('post')) {
            // check if file is uploaded
            if ($request->hasFile('file') && $request->file('file')->isValid()) {
                $request->flashOnly(['name']);

                // in-line file validation
                $vInput     =   $request->all();
                $vRules     =   [
                                    'file'  =>  'sometimes|mimes:txt,jpg,jpeg,png,pdf,doc,docx,xls|max:1024',
                                ];
                $vMessages  =   [
                                    'file.max'       =>     title_case('File is too large: Reduce size to 1MB or less.'),
                                    'file.mimes'     =>     title_case('Invalid file type: Please upload
                                                                only notepads, images, pdfs, word and excel files.')
                                ];      
                $validator  =   Validator::make($vInput, $vRules, $vMessages);

                if ($validator->fails()) {
                    return redirect(url('/#contact'))
                            ->withErrors($validator)
                            ->withInput();
                }

                //create a subdirectory sorted by date
                $dir            =       "uploads/" . str_before(today(), ' 00:00:00');
                $filename       =       $request->file->getclientOriginalName();
                $path           =       $request->file->storeAs($dir, $filename);

                // capture required POST vars
                $fields     =   (Object) array(
                                            'name'      =>  $request->name,
                                            'email'     =>  $request->email_two,
                                            'subject'   =>  $request->subject,
                                            'message'   =>  $request->message,
                                        );

                // dispatch to queue handler                
                WithFileJob::dispatch($fields, $filename, $path)
                                ->delay(now()->addSeconds(5));

                return redirect('emailsent');
                $request->session()->flush();
                $request->session()->regenerate();
            } elseif (
                        $request->has(['name', 'email_two', 'subject', 'message']) &&
                        $request->filled(['name', 'email_two', 'subject', 'message'])
                                        ) {
                $request->flashOnly(['name']);

                // capture required POST vars
                $fields   =       (Object) array(
                                            'name'      =>  $request->name,
                                            'email'     =>  $request->email_two,
                                            'subject'   =>  $request->subject,
                                            'message'   =>  $request->message,
                                        );

                // dispatch to queue handler
                NoFileJob::dispatch($fields)
                            ->delay(now()->addSeconds(5));

                return redirect('emailsent');
                $request->session()->flush();
                $request->session()->regenerate();
            }
        }
    }
}