<?php

namespace App\Jobs\ContactForm;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactForm\NoFileMailable;

class NoFileJob implements ShouldQueue
{
    private $fields;
    public $tries = 3, $timeout = 20;

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Object $fields)
    {
        $this->fields   =   $fields;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $form       =   (Object) array(
                            'name'       =>   $this->fields->name,
                            'email'      =>   $this->fields->email,
                            'subject'    =>   $this->fields->subject,
                            'message'    =>   $this->fields->message,
                                );

        Mail::to('enquiries@blueplush.com', 'Blue Plush')
                ->send(new NoFileMailable($form));
    }
}