<?php

namespace App\Jobs\ContactForm;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactForm\WithFileMailable;

class WithFileJob implements ShouldQueue
{
    private $fields, $filename, $path;
    public $tries = 3, $timeout = 20;

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Object $fields, $filename, $path)
    {
        $this->fields       =       $fields;
        $this->filename     =       $filename;
        $this->path         =       $path;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $form   =   (Object) array(
                            'name'      =>      $this->fields->name,
                            'email'     =>      $this->fields->email,
                            'subject'   =>      $this->fields->subject,
                            'message'   =>      $this->fields->message,
                        );

        $filename   =   $this->filename;
        $path       =   $this->path;

        Mail::to('enquiries@blueplush.com', 'Blue Plush')
                ->send(new WithFileMailable($form, $filename, $path));
    }
}